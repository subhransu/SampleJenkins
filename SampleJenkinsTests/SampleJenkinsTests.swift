//
//  SampleJenkinsTests.swift
//  SampleJenkinsTests
//
//  Created by Subhransu Behera on 18/8/16.
//  Copyright © 2016 Singapore Power. All rights reserved.
//

import XCTest
@testable import SampleJenkins

class SampleJenkinsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testHello() {
        XCTAssertEqual(1 + 1, 2, "one plus one should equal two")
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
